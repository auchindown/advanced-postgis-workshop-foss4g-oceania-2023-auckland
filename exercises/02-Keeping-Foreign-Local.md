Keeping Foreign Local: Travelling Without Moving
================================================

## The Scenario: How did you get here?

Your friends are very excited that you are going to FOSS4G 2023 in Auckland, New Zealand. They want to know which countries you travelled over to get here. However, on your way over on the plane, train or automobile, you fell asleep and didn't get to look at the lovely moving map display on said train, plane or automobile and thus can't tell them. Luckily, you are at a geospatial conference in a workshop on the best geospatial tool in existence.

## The Tools

So, to start, we need an additional set of data. We already have the path that was taken to get to Auckland from exercise 1. We need a list of all the countries on earth and their coordinates. Luckily, there are several free/public domain/open source datasets that we can use to assist us. We will be using the [Natural Earth Vector](https://www.naturalearthdata.com/) data.

We could download the data, open it in a [reputable desktop GIS](https::/www.qgis.org) and then import it into the `workshop` database. However, we will, as best as possible, seek to minimise the use of tools external to Postgresql/PostGIS and its immediate ecosystem. Thus, we will be using the [ogr_fdw Foreign Data Wrapper.](https://github.com/pramsey/pgsql-ogr-fdw)

## Solving it

ogr_fdw is packaged as a [Postgresql extension](https://www.postgresql.org/docs/current/sql-createextension.html). So let us enable it:

```SQL
CREATE EXTENSION ogr_fdw;
```

Once the extension is enabled we create a server.

```SQL
CREATE SERVER natural_earth FOREIGN DATA WRAPPER ogr_fdw
  OPTIONS (
    datasource '/vsizip//files/natural_earth_vector.gpkg.zip/packages/natural_earth_vector.gpkg/',
    format 'GPKG');
```

We still love our namespaces, so create a schema to hold this dataset.
```SQL
CREATE SCHEMA natural_earth;
```


Servers may have one or more tables, in this case, the geopackage has over 100 different tables. Normally, we would need to create each foreign table individually using `CREATE FOREIGN TABLE`....but [Aint no body got time for that](https://www.youtube.com/watch?v=waEC-8GFTP4&t=27s). Instead, we'll use what is arguably the best thing since the [best thing since sliced bread](https://www.postgresql.org/docs/current/storage-toast.html): `IMPORT FOREIGN SCHEMA`.

```SQL
IMPORT FOREIGN SCHEMA ogr_all
  FROM SERVER natural_earth INTO natural_earth
  OPTIONS (launder_table_names 'false', launder_column_names 'false');
```

This will create all the foreign tables found in the server `natural_earth` into the schema `natural_earth`. [You can import a subset of the tables by using rudimentary prefix macthing by changing ogr_all to the first couple letters of the tables that you are interested in.](https://github.com/pramsey/pgsql-ogr-fdw#automatic-foreign-table-creation) You can also use the standard `IMPORT FOREIGN SCHEMA` parameters `EXCEPT` AND `LIMIT TO`.

So, now we have lots of data. Lets put the two sets together. The table we are interested in is `ne_10m_admin_0_countries`. So we will do a spatial join using `st_intersects`. 

```SQL
SELECT * FROM recap.how_far_is_home
JOIN natural_earth.ne_10m_admin_0_countries ON st_intersects(geo,geom);
```

So, we get back several columns which we do not need. We just want the name of the country.

```SQL
SELECT "ADMIN" country FROM recap.how_far_is_home
JOIN natural_earth.ne_10m_admin_0_countries ON st_intersects(geo,geom);
```

You can also use `st_dwithin` and a distance parameter of 0 instead of `st_intersects`. Give it a try if you want.

## Bonus Points: Speeding it up

The query does take a little while. This is because we are querying a foreign table and, as per the [ogr_fdw docs](https://github.com/pramsey/pgsql-ogr-fdw#limitations), only bounding box filters are pushed down. So the best course of action is to create a regular table and index it:

```SQL
CREATE TABLE natural_earth.countries AS
  SELECT "ADMIN" country, "CONTINENT", "SUBREGION", "REGION_WB", "POP_EST", geom geo
  FROM natural_earth.ne_10m_admin_0_countries;
```

Lets now run the previous query but on the local table: 
```SQL
EXPLAIN ANALYZE SELECT country FROM recap.how_far_is_home a
JOIN natural_earth.countries b ON st_intersects(a.geo, b.geo);
```
Do you see any indication of the index being used?

Add a spatial index.
```SQL
CREATE INDEX ON natural_earth.countries USING gist (geo);
```

Re-run the previous explain quiery. Did the index make a difference? No? ogr_fdw defaults to the geometry type. Let us convert the geo column in the `natural_earth.countries` table to a geography type.

```SQL
ALTER TABLE natural_earth.countries ALTER COLUMN geo TYPE geography;
```

Does the index make a difference now?

## More Bonus Points.

Let us try and arrange the countries in the order in which they were passed over. There are a plethora of function in PostGIS. It is up to the user to have an understanding of what functions can do and how to mix and match them to get the desired answers. There is no `st_order_by_position_on_line` function (yet). So we will have to be a bit creative. What we want is the countries visited in order, so we basically want the country closest to where we started, followed by the next and so on. There is the `st_startpoint` function. Lets take advantage of that.

```SQL
SELECT country, st_distance(st_startpoint(a.geo), b.geo) FROM recap.how_far_is_home a
JOIN natural_earth.countries b ON st_intersects(a.geo, b.geo)
ORDER BY 2;  -- ORDER BY 2 is a shortcut so we do not have to write out a column name or an entire expression
```

This should give you an error saying that `ERROR:  function st_startpoint(geography) does not exist`. And that is because st_startpoint takes a geometry as its only argument. If you have psql open, type `\df st_startpoint`. You will see that there is one function that accepts and returns a gemetry type. So we have to cast our geography to a geometry.

```SQL
SELECT country, st_distance(st_startpoint(a.geo::geometry), b.geo) FROM recap.how_far_is_home a
JOIN natural_earth.countries b ON st_intersects(a.geo,b.geo)
ORDER BY 2;  -- ORDER BY 2 is a shortcut so we do not have to write out a column name or an entire expression
```

This works, but its all getting a bit wordy.

## Going overboard for a recap

Let's create our own `st_startpoint` function that accepts and returns a geography type.

```SQL
CREATE FUNCTION st_startpoint(IN geog GEOGRAPHY) RETURNS geography
RETURN (SELECT st_startpoint(geog::geometry)::geography);
COMMENT ON FUNCTION st_startpoint(geography) IS 
    'An st_startpoint for the geography type.';
```
Note that this is using the new create function syntax that is valid as at Postgresql 14. If you type `\df+ st_startpoint` in psql, you should now see both functions.

And finally, let's create a new table with the info for posterity:

```SQL
CREATE TABLE recap.countries_i_flew_over_to_get_here AS
SELECT country, st_distance(st_startpoint(a.geo), b.geo) FROM recap.how_far_is_home a
JOIN natural_earth.countries b ON st_intersects(a.geo, b.geo)
ORDER BY 2;
```

Wow, thats great, look at you travelling without moving! Lets travel to the [next exercise](./03-Validating-Geometric-Constraints.md) where we will look at geometries and having them not bite you where it hurts: your data.

