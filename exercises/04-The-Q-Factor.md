The Elephant in the Q: Usefully Deep Integerations with QGIS
============================================================

## Preamble

So far, we've been using PSQL (or pg_admin or dbeaver, or maybe evn QGIS' DB Manager) to look at data and execute queries. This, however, has limitations. At some point, we will need to visualize data, either for ourselves or others. QGIS has some very useful integrations with postgis.

## Connecting to the database

Open QGIS. Invoke the `Data Source Manager` either by doing `CTRL+SHIFT+D` or going through the menus `Layer->Data Source Manager`. And then select the PostgreSQL tab. Click the `New` button and the following should pop up:

![QGIS PostgreSQL Connection Dialog](.img/04_qgis_postgis_connection_dialog.png "QGIS PostgreSQL Connection Dialog")

You can fill in the supplied credentials for connection name, host, database etc. The really cool parts are the checkboxes below the `Test Connection` button. All but the 1st & 3rd options should be checked.

## Project Properties

Invoke the `Project Properties` dialog either by doing `CTRL+SHIFT+P` or going through the menus `Project->Properties`.
Select the `Data Sources` tab. You should see the following:

![QGIS PostgreSQL Connection Dialog](.img/04_qgis_project_properties_dialog.png "QGIS Project Properties")

Transaction mode should be set to `Automatic Transaction Groups` and the `Evaluate Default Values on Provider Side` checkbox should be checked. With the transaction mode selected, all edits are pushed to the database as they are made, this enables any triggers or rules to be fired and the effects of the triggers or rules are seen in real time. Note however, that the changes are made in transaction, and until the changes are commiited, no one else will see them. And by committed I mean that you pressed the save edits button in QGIS.

## Layer Properties

And finally, let us load the `natural_earth.countries` table from the last exercise. You can either look in the browser panel or go through the `Data Source Manager`. Once you have added the layer, right click on it and select `properties` and select the `Rendering` tab.

![QGIS PostgreSQL Connection Dialog](.img/04_qgis_layer_properties.png "QGIS Layer Properties")

The `Refresh layer on notification` option is very useful and we will be exploring it later in the workshop.

Ok, with that out of the way lets jump into our first [real scenario](./05-Rasterman-Vibrations.md)
