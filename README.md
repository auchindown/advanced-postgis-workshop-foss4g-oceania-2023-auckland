Advanced Postgis Workshop FOSS4G SotM Oceania - Auckland
========================================================

Requirements
------------

 - Git.
 - Containers. Docker/Podman. If able to orchestrate a docker-compose.yml file then you are on the right track.
 - A network connection.

Getting Started
---------------

 1. Clone this repository.
 2. Execute `docker compose up` or `podman-compose up`.
 3. Confirm that there are three containers running using `docker ps` or `podman ps`.

That should be it. You can connect to the database with the following connection string:
`postgres://workshop@localhost:44449/workshop`. Or if you are on a *nix system run the `dbin.sh` to pop you stright into the database (this will only work if you have psql already installed.)

Also if you visit `localhost:44440` you should see a web page with a link back to this repository and to web pages that pertain to some exercises and the postgrest server.

If all the sanity checks worked out, you can pop over to [Getting Started](exercises/00-Getting-Started.md).
